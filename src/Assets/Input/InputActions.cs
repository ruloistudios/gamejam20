// GENERATED AUTOMATICALLY FROM 'Assets/Input/InputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActions"",
    ""maps"": [
        {
            ""name"": ""PlayerControlls"",
            ""id"": ""99d627e4-c10d-48b8-a3d6-b10be19848a7"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""7700e33b-5e66-4290-9a5c-1aeb6b4c9865"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FireDirection"",
                    ""type"": ""PassThrough"",
                    ""id"": ""73251c2c-cfff-4695-9092-31baa03ed6f1"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""PassThrough"",
                    ""id"": ""198d921f-3bac-4a61-831e-bc0714090615"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Shoot"",
                    ""type"": ""Button"",
                    ""id"": ""24b16058-0dff-434e-a949-88f6fa6b92ad"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Use"",
                    ""type"": ""Button"",
                    ""id"": ""ad781486-4a5d-4899-ba99-4c3c7df21f36"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Leave"",
                    ""type"": ""Button"",
                    ""id"": ""18439fea-b8b7-4c23-a049-529855093be2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Leave1"",
                    ""type"": ""Button"",
                    ""id"": ""7ed07713-ac18-46de-8759-6d748fe3aa9a"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9d397a6a-1393-462b-aa65-fd98ecbef59c"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""06762e6a-5d34-4656-9f6d-e0dcb83ae766"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""a1e88e13-6f35-4a6c-aaa1-89fcbb78d944"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""d34aa32b-3ec9-4d7a-a1fa-4007a69b4c93"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""2f0ede6e-265f-4dec-b179-8d79b09dae02"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""7662fa8e-69a7-4c96-bfc0-17e7e8a0fba2"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""8d97a147-1b20-4bd8-881d-a4ab0bcacc06"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FireDirection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Arrows"",
                    ""id"": ""3e3e5f6c-5dc2-49b6-9b14-f8173401ad9c"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FireDirection"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""c60b9f16-30e8-46de-81ae-01b929c0316a"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FireDirection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""757d43e9-074a-4ce5-950f-879b4f7ebf73"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FireDirection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""26b069bd-9474-4f8e-bd9e-e1b7daa0f9a8"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FireDirection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""f896f37a-50b1-4826-9902-fd33ecd083f1"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FireDirection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""d1d36795-544f-4f9f-9a30-351e392f9053"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d7fa1b5b-b8c0-4a7c-a098-3bc97ea998e9"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eaae31f1-b21a-45dd-a8ce-decf6eba3fa3"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Shoot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8d2e993b-c86d-471d-9978-fea5e7178321"",
                    ""path"": ""<XInputController>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Shoot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d2738a96-195d-4924-b074-82fbcfde07b8"",
                    ""path"": ""<XInputController>/rightTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Shoot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""815c08c0-9e34-43b5-99ad-19f96c423453"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Use"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""abc4d971-034e-4c15-bbcd-691f79b31645"",
                    ""path"": ""<XInputController>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Use"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""072ce483-77ed-4f66-a931-98f6fac5968c"",
                    ""path"": ""<XInputController>/leftTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Use"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""51508f13-1681-4ce2-9e21-be3d551ad950"",
                    ""path"": ""<Keyboard>/p"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Leave"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d341ccb5-7570-4c44-b9f1-dd400a119d38"",
                    ""path"": ""<XInputController>/start"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Leave"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d85bd9d3-2542-4cc6-a1aa-e00f2189322f"",
                    ""path"": ""<Keyboard>/p"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Leave1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f4e69ba2-f8bf-4722-a2a5-4fd6324b4a3a"",
                    ""path"": ""<XInputController>/start"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Leave1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // PlayerControlls
        m_PlayerControlls = asset.FindActionMap("PlayerControlls", throwIfNotFound: true);
        m_PlayerControlls_Move = m_PlayerControlls.FindAction("Move", throwIfNotFound: true);
        m_PlayerControlls_FireDirection = m_PlayerControlls.FindAction("FireDirection", throwIfNotFound: true);
        m_PlayerControlls_Jump = m_PlayerControlls.FindAction("Jump", throwIfNotFound: true);
        m_PlayerControlls_Shoot = m_PlayerControlls.FindAction("Shoot", throwIfNotFound: true);
        m_PlayerControlls_Use = m_PlayerControlls.FindAction("Use", throwIfNotFound: true);
        m_PlayerControlls_Leave = m_PlayerControlls.FindAction("Leave", throwIfNotFound: true);
        m_PlayerControlls_Leave1 = m_PlayerControlls.FindAction("Leave1", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerControlls
    private readonly InputActionMap m_PlayerControlls;
    private IPlayerControllsActions m_PlayerControllsActionsCallbackInterface;
    private readonly InputAction m_PlayerControlls_Move;
    private readonly InputAction m_PlayerControlls_FireDirection;
    private readonly InputAction m_PlayerControlls_Jump;
    private readonly InputAction m_PlayerControlls_Shoot;
    private readonly InputAction m_PlayerControlls_Use;
    private readonly InputAction m_PlayerControlls_Leave;
    private readonly InputAction m_PlayerControlls_Leave1;
    public struct PlayerControllsActions
    {
        private @InputActions m_Wrapper;
        public PlayerControllsActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_PlayerControlls_Move;
        public InputAction @FireDirection => m_Wrapper.m_PlayerControlls_FireDirection;
        public InputAction @Jump => m_Wrapper.m_PlayerControlls_Jump;
        public InputAction @Shoot => m_Wrapper.m_PlayerControlls_Shoot;
        public InputAction @Use => m_Wrapper.m_PlayerControlls_Use;
        public InputAction @Leave => m_Wrapper.m_PlayerControlls_Leave;
        public InputAction @Leave1 => m_Wrapper.m_PlayerControlls_Leave1;
        public InputActionMap Get() { return m_Wrapper.m_PlayerControlls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerControllsActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerControllsActions instance)
        {
            if (m_Wrapper.m_PlayerControllsActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnMove;
                @FireDirection.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnFireDirection;
                @FireDirection.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnFireDirection;
                @FireDirection.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnFireDirection;
                @Jump.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnJump;
                @Shoot.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnShoot;
                @Shoot.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnShoot;
                @Shoot.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnShoot;
                @Use.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnUse;
                @Use.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnUse;
                @Use.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnUse;
                @Leave.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnLeave;
                @Leave.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnLeave;
                @Leave.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnLeave;
                @Leave1.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnLeave1;
                @Leave1.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnLeave1;
                @Leave1.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnLeave1;
            }
            m_Wrapper.m_PlayerControllsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @FireDirection.started += instance.OnFireDirection;
                @FireDirection.performed += instance.OnFireDirection;
                @FireDirection.canceled += instance.OnFireDirection;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Shoot.started += instance.OnShoot;
                @Shoot.performed += instance.OnShoot;
                @Shoot.canceled += instance.OnShoot;
                @Use.started += instance.OnUse;
                @Use.performed += instance.OnUse;
                @Use.canceled += instance.OnUse;
                @Leave.started += instance.OnLeave;
                @Leave.performed += instance.OnLeave;
                @Leave.canceled += instance.OnLeave;
                @Leave1.started += instance.OnLeave1;
                @Leave1.performed += instance.OnLeave1;
                @Leave1.canceled += instance.OnLeave1;
            }
        }
    }
    public PlayerControllsActions @PlayerControlls => new PlayerControllsActions(this);
    public interface IPlayerControllsActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnFireDirection(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnShoot(InputAction.CallbackContext context);
        void OnUse(InputAction.CallbackContext context);
        void OnLeave(InputAction.CallbackContext context);
        void OnLeave1(InputAction.CallbackContext context);
    }
}
