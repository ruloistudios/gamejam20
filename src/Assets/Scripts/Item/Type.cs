namespace Item
{
    public enum Type
    {
        Canon = 1,
        Pistol = 2,
        Lighter = 3,
    }
}