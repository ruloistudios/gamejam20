using System;
using DefaultNamespace;
using GameManagers;
using Scriptables;
using UnityEngine;

namespace Item
{
    public class Repairable : MonoBehaviour
    {
        public int maxLife;
        public Interactuable item;
        private int currentLife;

        public Team t;
        public GameObject[] props;
        public InteractableItemHUDView hud;
        public void Awake()
        {
            currentLife = maxLife / 2;
            SetProps();
            hud = GetComponentInChildren<InteractableItemHUDView>();
        }

        public void Repair(Resource.Collection have)
        {
            if (!Damaged())
            {
                return;
            }

            Resource.Collection needs = new Resource.Collection();
            Resource.Collection needsCopy = new Resource.Collection();
            
            foreach (var need in item.neededToRepair)
            {
                needs.Give(need, 1);
                needsCopy.Give(need, 1);
            }
            
            needs.Substract(have);

            if (!needs.Empty())
            {
                return;
            }
     
            currentLife = maxLife;
            have.Substract(needsCopy);
            SetProps();
            BoatsController.instance.OnRepairableStateChanged();
        }

        public void TakeDamage(int amount)
        {
            currentLife -= amount;
            currentLife = currentLife < 0 ? 0 : currentLife;

            SetProps();
            BoatsController.instance.OnRepairableStateChanged();
        }

        public void SetProps()
        {
            props[0].SetActive(DamagePercent() >= 0.25f);
            props[1].SetActive(DamagePercent() >= 0.5f);
            props[2].SetActive(DamagePercent() >= 0.75f);
        }

        public bool Broken()
        {
            return currentLife == 0;
        }

        public bool Damaged()
        {
            return currentLife != maxLife;
        }

        public int Damage()
        {
            return maxLife-currentLife;
        }

        public float DamagePercent()
        {
            return 1 - (currentLife / (float)maxLife);
        }

        public int CurrentLife()
        {
            return currentLife;
        }
        
        public int MaxLife()
        {
            return maxLife;
        }
    }
}