using UnityEngine;

namespace DefaultNamespace
{
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager instance;
        public AudioSource[] audioSources;

        public AudioClip soundTrack;

        public AudioClip parrot,
            resourceCollected,
            cannonShot,
            gunshot,
            gameOver,
            gameStart,
            hello,
            hey,
            byebye,
            pirateArr,
            screamMale,
            laugh,
            diesInWater,
            glassEye,
            step,
            waterSplash;

        void Awake()
        {
            instance = this;
        }

        void Start()
        {
            AudioSource source = GetAudioSource();
            source.clip = soundTrack;
            source.loop = true;
            source.Play();
        }

        public void ShipPartBroken()
        {
            switch (Random.Range(0,2))
            {
                case 0:
                    PlaySound(glassEye);
                    break;
                case 1:
                    PlaySound(laugh);
                    break;
            }
        }

        public void PlayerWalk()
        {
            PlaySound(step);
        }
        
        public void PlayerDies()
        {
            switch (Random.Range(0,2))
            {
                case 0:
                    PlaySound(byebye);
                    break;
                case 1:
                    PlaySound(screamMale);
                    break;
            }
        }

        public void PlayerDiesInWater()
        {
            switch (Random.Range(0,2))
            {
                case 0:
                    PlaySound(diesInWater);
                    break;
                case 1:
                    PlaySound(waterSplash);
                    break;
            }
        }

        public void PlayerJoined()
        {
            switch (Random.Range(0,3))
            {
                case 0:
                    PlaySound(hey);
                    break;
                case 1:
                    PlaySound(hello);
                    break;
                case 2:
                    PlaySound(pirateArr);
                    break;
            }
        }
        public void GameStart()
        {
            PlaySound(gameStart);
        }
        public void GameOver()
        {
            PlaySound(gameOver);
        }
        public void GunShot()
        {
            PlaySound(gunshot);
        }

        public void CannonShot()
        {
            PlaySound(cannonShot);
        }

        public void ResourceCollected()
        {
            PlaySound(resourceCollected);
        }
        
        public void ParrotStart()
        {
            PlaySound(parrot);
        }

        void PlaySound(AudioClip clip)
        {
            AudioSource audio = GetAudioSource();
            audio.loop = false;
            audio.clip = clip;
            audio.Play();
        }

        private AudioSource GetAudioSource()
        {
            for (int i = 0; i < audioSources.Length; i++)
            {
                if (!audioSources[i].isPlaying)
                {
                    return audioSources[i];
                }
            }

            return audioSources[Random.Range(0, audioSources.Length)];
        }

    }
}