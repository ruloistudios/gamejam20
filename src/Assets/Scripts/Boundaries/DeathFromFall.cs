using System;
using DefaultNamespace;
using UnityEngine;

namespace Boundaries
{
    public class DeathFromFall : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Pirate p = other.GetComponentInChildren<Pirate>();
                p.ReceiveDamage(1000);
                GameController.Instance.KillPirate(p);
                SoundManager.instance.PlayerDiesInWater();
            }
        }
    }
}