using System.Collections;
using System.Collections.Generic;
using GameManagers;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerHUDView : MonoBehaviour
{
    public Slider healthBar;
    public Image playerImage;
    public Transform itemsHolder;
    public List<ItemView> items = new List<ItemView>();
    public Pirate pirate;
    public PlayerInput pInput;

    public Sprite[] portraits;
    // Start is called before the first frame update

    public void Init(Pirate p, PlayerInput pInput)
    {
        this.pInput = pInput;
        pirate = p;
        Refresh();
        playerImage.sprite = portraits[pInput.playerIndex];
    }

    public void Refresh()
    {
        ClearAllItems();
        if (pirate.inventory != null)
        {
            foreach (var res in pirate.inventory.resources)
            {
                SpawnItem(res.Key, res.Value);
            }
        }

        SetHealthBar();

    }

    void SetHealthBar()
    {
        healthBar.value = pirate.currentHealth / (float) pirate.maxHealth;
    }

    void SpawnItem(Resource.Type t, int amount)
    {
        ItemView item = Instantiate(UIPrefabsHolder.instance.itemViewPrefab, itemsHolder);
        items.Add(item);
        item.Init(t, amount);
    }

    void ClearAllItems()
    {
        GameObject go;
        while (items.Count > 0)
        {
            go = items[0].gameObject;
            items.RemoveAt(0);
            Destroy(go);
        }
    }
}