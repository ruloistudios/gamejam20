using System.Collections;
using System.Collections.Generic;
using GameManagers;
using Item;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.UI;
using Type = Resource.Type;

public class InteractableItemHUDView : MonoBehaviour
{
    public Slider healthBar;
    public Transform itemsHolder;
    public List<ItemView> items = new List<ItemView>();
    public Team team;
    public Repairable repairable;
    public TMP_Text repairText;
    public SpriteRenderer hammerImage;

    // Start is called before the first frame update

    public void Start()
    {
        Refresh();
    }
    
    public void Refresh()
    {
        gameObject.SetActive(repairable.Damaged());
        repairText.gameObject.SetActive(repairable.Damaged());
        hammerImage.gameObject.SetActive(repairable.Damaged());
        
        ClearAllItems();
        SetNecessaryItems();
        SetHealthBar();

    }

    void SetNecessaryItems()
    {
        
        for (int i = 0; i < repairable.item.neededToRepair.Length; i++)
        {
            SpawnItem(repairable.item.neededToRepair[i]);
        }
    }

    void SpawnItem(Type resType)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].resType == resType)
            {
                items[i].IncreaseAmount();
                return;
            }
        }

        ItemView iv = Instantiate(UIPrefabsHolder.instance.itemViewPrefab, itemsHolder);
        items.Add(iv);
        iv.Init(resType,1);
    }

    void SetHealthBar()
    {
        healthBar.value = repairable.CurrentLife() / (float) repairable.MaxLife();
    }

    void ClearAllItems()
    {
        GameObject go;
        while (items.Count > 0)
        {
            go = items[0].gameObject;
            items.RemoveAt(0);
            Destroy(go);
        }
    }
}