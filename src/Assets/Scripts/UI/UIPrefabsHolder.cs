using System;
using UnityEngine;

namespace UI
{
    public class UIPrefabsHolder : MonoBehaviour
    {
        public static UIPrefabsHolder instance;

        public ItemView itemViewPrefab;
        public PlayerHUDView playerHUDPrefab;
        private void Awake()
        {
            instance = this;
        }
    }
}