﻿using System.Collections;
using System.Collections.Generic;
using GameManagers;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.UI;

public class ItemView : MonoBehaviour
{
    public TMP_Text amountText;

    public Image itemImage;

    public Resource.Type resType;

    public int amount;
    // Start is called before the first frame update

    public void Init(Resource.Type type, int amout)
    {
        this.amount = amout;
        resType = type;
        amountText.text = amout.ToString();
        itemImage.sprite = CollectionManager.Instance.resources.GetResource(type).sprite;
    }

    public void IncreaseAmount(int amount = 1)
    {
        this.amount += amount;
        amountText.text = this.amount.ToString();
    }
}
