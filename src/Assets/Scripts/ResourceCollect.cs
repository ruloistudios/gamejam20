﻿using System;
using System.Collections;
using DefaultNamespace;
using RepairPirates;
using UnityEngine;

public class ResourceCollect : MonoBehaviour
{
    public Resource.Type type;
    public int amount;
    public Sprite sprite;

    public void Init(Resource.Type t, int a, Sprite s)
    {
        type = t;
        amount = a;
        sprite = s;
        gameObject.GetComponent<SpriteRenderer>().sprite = s;

        StartCoroutine(TTL());
    }
    
    private IEnumerator TTL()
    {
        yield return new WaitForSeconds(Config.RESOURCES_TTL);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.GetComponent<Collider2D>().CompareTag("Player"))
        {
            return;
        }
        
        SoundManager.instance.ResourceCollected();
        Pirate who = collision.GetComponent<Pirate>();

        if (type != Resource.Type.Parrot) {
            who.PickResource(type, amount);
            Destroy(gameObject);
            return;
        }
        
        var parrotPrefab = Resources.Load<Parrot>("parrot");
        var parrot = Instantiate(parrotPrefab, transform.position, Quaternion.identity);
        Pirate enemy = null;

        for (int i = 0; i < GameController.Instance.players.Count; i++)
        {
            var p = GameController.Instance.players[i];
            if (p.team != who.team && p.currentHealth > 0)
            {
                enemy = p;
                break;
            }
        }

        if (enemy == null)
        {
            Debug.LogError("parrot, no enemy was found");
            return;
        }

        parrot.Init(who, enemy);
        Destroy(gameObject);
    }
}
