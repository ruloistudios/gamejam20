using UnityEngine;

namespace Scriptables
{
    [CreateAssetMenu(fileName = "Interactuable", menuName = "ScriptableObjects/Interactuable", order = 1)]
    
    public class Interactuable : ScriptableObject
    {
        public Sprite itemImage;
        public Item.Type itemType;
        public Resource.Type[] neededToRepair;
    }
}