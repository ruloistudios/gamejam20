using UnityEngine;

namespace Scriptables
{
    [CreateAssetMenu(fileName = "ResourceCollection", menuName = "ScriptableObjects/ResrouceCollection", order = 1)]
    
    public class ResourceScriptableCollection : ScriptableObject
    {
        public ResourceScriptable[] resources;

        public ResourceScriptable GetResource(Resource.Type type)
        {
            for (int i = 0; i < resources.Length; i++)
            {
                if (resources[i].type == type)
                {
                    return resources[i];
                }
            }

            return resources[0];
        }
        
        public ResourceScriptable GetRandomResource()
        {
            int pick = Random.Range(0, resources.Length);
            return resources[pick];
        }
    }
}