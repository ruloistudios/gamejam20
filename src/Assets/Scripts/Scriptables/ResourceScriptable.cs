using UnityEngine;

namespace Scriptables
{
    [CreateAssetMenu(fileName = "Resource", menuName = "ScriptableObjects/Resource", order = 1)]
    
    public class ResourceScriptable : ScriptableObject
    {
        public Resource.Type type;
        public Sprite sprite;
        public string name;
        public AudioClip audio;
    }
}