using UnityEngine;

namespace Scriptables
{
    [CreateAssetMenu(fileName = "InteractuableCollection", menuName = "ScriptableObjects/InteractuableCollection", order = 1)]
    
    public class InteractuableCollection : ScriptableObject
    {
        public Interactuable[] items;
    }
}