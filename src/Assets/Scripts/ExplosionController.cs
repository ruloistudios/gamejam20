using System;
using System.Collections;
using RepairPirates;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
    public void Init()
    {
        StartCoroutine(TTL());
    }

    public void Update()
    {
    }

    private IEnumerator TTL()
    {
        yield return new WaitForSeconds(Config.EXPLOSION_TTL);
        Destroy(gameObject);
    }
}