using System;
using Item;
using UnityEngine;
using Weapon;

public class PirateUseController : MonoBehaviour
{
    private Repairable lastInteractuable;
    public Pirate p;
    public Pistol pistol;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Destructible"))
        {
            lastInteractuable = collision.transform.GetComponent<Repairable>();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        lastInteractuable = null;
    }

    public void Use()
    {
        if (lastInteractuable != null)
        {
            //Debug.Log("Using repairable: " + lastInteractuable.gameObject.name);
            if(!lastInteractuable.Damaged())
                return;
            lastInteractuable.Repair(p.inventory);
            p.hudView.Refresh();
            lastInteractuable.hud.Refresh();
        }
    }

    public void Shoot()
    {
        pistol.Shoot();
    }
}
