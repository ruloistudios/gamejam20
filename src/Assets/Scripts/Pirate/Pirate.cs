﻿using System.Collections;
using DefaultNamespace;
using RepairPirates;
using UnityEngine;
using UnityEngine.UI;
using Weapon;

public enum Team
{
    Green = 1,
    Red = 2
};
public class Pirate : MonoBehaviour
{
    public int maxHealth = 20;
    public int currentHealth;
    
    public PlayerController pc;
    public Team team;
    public Image playerImage;
    public Resource.Collection inventory = new Resource.Collection();
    public PlayerHUDView hudView;
    public Pistol pistol;
    public PirateUseController useController;
    
    public bool isFacingLeft = false;

    public bool IsFacingLeft
    {
        get => isFacingLeft;
        set => isFacingLeft = value;
    }

    public void Init()
    {
        maxHealth = 20;
        currentHealth = maxHealth;
    }
    
    public void ReceiveDamage(int amount)
    {
        currentHealth -= amount;

        if (currentHealth <= 0)
        {
            Die();
        }
        
        hudView.Refresh();
    }

    public void PickResource(Resource.Type type, int amount)
    {
        if (type == Resource.Type.Parrot)
        {
            return;
        }
        
        inventory.Give(type, amount);
        hudView.Refresh();
    }

    public void Die()
    {
        SoundManager.instance.PlayerDies();
        GameController.Instance.KillPirate(this);
    }
    
    void SetTeamColor()
    {
        if (team == Team.Green)
        {
            playerImage.color = Color.blue;
        }
        else
        {
            playerImage.color = Color.red;
        }
    }

    public void Use()
    {
        useController.Use();   
    }

    public void Shoot()
    {
        useController.Shoot();
    }

    public PlayerController GetPlayerController()
    {
        return pc;
    }

}
