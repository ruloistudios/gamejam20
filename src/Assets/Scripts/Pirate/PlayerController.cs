﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.InputSystem;
using RepairPirates;

public class PlayerController : MonoBehaviour
{
    // Private
    private InputActions inputActions;
    private Vector2 movementInput;
    private float yMovementInput;
    private Vector2 lookInput;

    private bool jumpJustPressed;
    private bool jumpJustReleased;

    private bool isGrounded;
    private bool isMoveBlock;

    private Rigidbody2D rigid;

    public Pirate pirate;

    private Animator anim;

    public PlayerInput pInput;

    public List<GameObject> possiblePlayers;

    private void Awake()
    {
        rigid = GetComponentInChildren<Rigidbody2D>();
        
        for (int i = 0; i < possiblePlayers.Count; i++)
        {
            possiblePlayers[i].SetActive(false);
        }
        anim = possiblePlayers[pInput.playerIndex].GetComponent<Animator>();

        possiblePlayers[pInput.playerIndex].SetActive(true);
    }

    void OnUse()
    {
        if(!GameController.Instance.isGameStarted)
            return;

        //Debug.Log("Using!");
        pirate.Use();
    }

    void OnShoot()
    {
        if(!GameController.Instance.isGameStarted)
            return;

        pirate.Shoot();
        anim.SetTrigger("shoot");
    }

    void OnLeave()
    {
        
        return;
        
        GameController.Instance.OnPlayerLeft(pInput);
        Destroy(this.gameObject);
    }

    void OnFireDirection(InputValue value)
    {
        
        #if UNITY_WEBGL
        var fireInput = value.Get<Vector2>().x;
        var jumpInput = value.Get<Vector2>().y;
        if (jumpInput > 0.8f)
        {
            OnJump();
        }
        if(fireInput < 0.8f)
            OnShoot();
        if (fireInput > 0.8f)
        {
            OnUse();
        }
        #endif
        
    }

    void OnMove(InputValue value)
    {

        if(!GameController.Instance.isGameStarted)
            return;

        float scale = pirate.transform.GetChild(0).localScale.y;
        movementInput = new Vector2(value.Get<Vector2>().x, 0);
        yMovementInput = value.Get<Vector2>().y;
        if (movementInput.x < 0)
        {
            pirate.IsFacingLeft = true;

            possiblePlayers[pInput.playerIndex].transform.localScale = new Vector3(scale, scale, scale);
            
            anim.SetBool("isWalking", true);
        }
        else if(movementInput.x > 0)
        {
            pirate.IsFacingLeft = false;
            possiblePlayers[pInput.playerIndex].transform.localScale = new Vector3(-scale, scale, scale);

            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }
    }

    void OnJump()
    {
        if (!GameController.Instance.isGameStarted)
        {
            return;
        }

        if (jumpJustPressed)
        {
            jumpJustPressed = false;
            jumpJustReleased = true;
        }
        else
        {
            jumpJustPressed = true;
            jumpJustReleased = false;
        }
        // Player only jumps when grounded
        if (isGrounded && jumpJustPressed)
        {
            if (yMovementInput > -0.9f)
            {
                rigid.AddForce(new Vector3(0, Config.JUMP_FORCE), ForceMode2D.Impulse);
                anim.SetTrigger("jump");
                isGrounded = false;
            }
            else
            {
                JumpDown();
            }

            SoundManager.instance.PlayerWalk();
        }
        //Debug.Log("Jumping");
    }

    void JumpDown()
    {
        if (!GameController.Instance.isGameStarted)
        {
            return;
        }

        //Debug.Log("Jumping down");
        StartCoroutine(JumpDownRoutine());
    }

    IEnumerator JumpDownRoutine()
    {
        SetLayerRecursively(8);
        yield return new WaitForEndOfFrame();
        rigid.AddForce(new Vector3(0, -0.001f), ForceMode2D.Impulse);
        yield return new WaitForSeconds(0.15f);
        SetLayerRecursively(0);
    }
    
    public void SetLayerRecursively(int layerNumber)
    {
        foreach (Transform trans in GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.layer = layerNumber;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoveBlock)
            return;

        if (!GameController.Instance.isGameStarted)
        {
            return;
        }

        transform.Translate(movementInput * Time.deltaTime * Config.LATERAL_MOVEMENT);
        

        if (rigid.velocity.y < 0 || jumpJustReleased) 
        {
            rigid.velocity += Vector2.up * Physics2D.gravity.y * 2.5f * Time.deltaTime;
        }
        else
        {
            rigid.velocity += Vector2.up * Physics2D.gravity.y * 0.5f * Time.deltaTime;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Platform")
        {
            isGrounded = true;
        }
    }



    /// <summary>
    /// Call this coroutine to freeze the player. The controls are disabled.
    /// </summary>
    /// <returns></returns>
    public IEnumerator WaitForBlockMove()
    {
        yield return new WaitForSeconds(Config.BLOCK_TIME);

        isMoveBlock = false;
    }

    public void ReceiveDamage(Transform t_other, int damage)
    {
        // Create the damage direction and apply the damage force
        Vector2 damage_direction = (t_other.position - transform.position).normalized;
        rigid.AddForce(damage_direction*damage, ForceMode2D.Impulse);
    }

    public void BlockMove()
    {
        isMoveBlock = true;
        
        StartCoroutine(WaitForBlockMove());
        
    }
}
