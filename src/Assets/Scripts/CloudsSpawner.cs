﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudsSpawner : MonoBehaviour
{
    public List<GameObject> clouds;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoopSpawn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator LoopSpawn()
    {
        while (true)
        {
            Vector2 spawnpos = transform.position + new Vector3(0, Random.Range(-2f, 2f));
            GameObject cloud = Instantiate(clouds[Random.Range(0, clouds.Count)] as GameObject, spawnpos, Quaternion.identity);
            cloud.transform.localScale *= Random.Range(0.5f, 1.5f);

            // Wait before the next cloud
            yield return new WaitForSeconds(Random.Range(0f, 3f));
        }
    }
}
