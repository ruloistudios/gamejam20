using System;
using System.Security.Cryptography;
using DefaultNamespace;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

// Author: http://luminaryapps.com/blog/arcing-projectiles-in-unity/
public class Parrot : MonoBehaviour
{
    public Pirate master;
    public Pirate enemy;
    private Resource.Type stolenGoodsType;
    private int stolenGoodsAmount;
    private bool flyToMaster; 

    public void Init(Pirate ms, Pirate en)
    {
        master = ms;
        enemy = en;
        
        SoundManager.instance.ParrotStart();
    }

    void Start()
    {
    }

    void Update()
    {
        var to = flyToMaster ? master : enemy;
        if (to == null)
        {
            Debug.LogError("parrot, no target, killing it");
            Vanish();
            return;
        }

        var dst = to.transform.position;
        transform.position = Vector3.MoveTowards(transform.position, dst, .11f);
        var dstToTarget =  Vector3.Distance(dst, transform.position);
        var arrival = dstToTarget < 0.3f;

        var direction = dst - transform.position;
        var lookingRight = direction.x > 0;
        var scale = transform.localScale;
        var mustRotate = (lookingRight && scale.x > 0) || (!lookingRight && scale.x < 0);
        
        if (mustRotate)
        {
            scale.x *= -1;
            transform.localScale = scale;
        }

        if (arrival)
        {
            if (flyToMaster)
            {
                DeliverStolenGoods();
            }
            else
            {
                StealGoods();
            }
        }
    }
	
    void DeliverStolenGoods()
    {
        if (stolenGoodsAmount <= 0)
        {
            return;
        }
        
        master.inventory.Give(stolenGoodsType, stolenGoodsAmount);
        master.hudView.Refresh();
        stolenGoodsAmount = 0;
        Vanish();
    }

    void StealGoods()
    {
        if (flyToMaster)
        {
            return;
        }

        if (stolenGoodsAmount > 0)
        {
            return;
        }

        flyToMaster = true;
        
        foreach (var res in enemy.inventory.resources)
        {
            stolenGoodsType = res.Key;
            stolenGoodsAmount = res.Value;
            enemy.inventory.Substract(stolenGoodsType, stolenGoodsAmount);
            enemy.hudView.Refresh();
            return;
        }
        
        // enemy has no resources, steal a random fake
        var values = Enum.GetValues(typeof(Resource.Type));
        var random = new System.Random();
        stolenGoodsType = (Resource.Type)values.GetValue(random.Next(values.Length));
        stolenGoodsAmount = 2;
    }

    void Vanish()
    {
        destroy();
    }

    void destroy()
    {
        Destroy(gameObject);
    }
}