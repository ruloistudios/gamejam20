﻿using System.Collections;
using System.Collections.Generic;
using Item;
using UnityEngine;

public class DamageObject : MonoBehaviour
{
    
    public Team team;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(team == collision.GetComponent<Pirate>().team)
                return;
            
            Pirate p = collision.GetComponent<Pirate>();
            p.GetPlayerController().ReceiveDamage(this.transform, 3);
            p.ReceiveDamage(3);
            Destroy(this.gameObject);
        }
        
    }
}