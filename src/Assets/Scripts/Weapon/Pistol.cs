using DefaultNamespace;
using Item;
using UnityEngine;

namespace Weapon
{
    public class Pistol : MonoBehaviour
    {
        public Bullet bulletPrefab;
        public Pirate p;
        
        public void Shoot()
        {
            /*
           if (!CanShoot())
               return;

           ConsumeResourcesToShoot();
           */

            SoundManager.instance.GunShot();
            
            Bullet b = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            b.team = p.team;
            if (p.IsFacingLeft)
            {
                b.Init(Vector2.left);
            }
            else
            {
                b.Init(Vector2.right);
            }
        }

        bool CanShoot()
        {
            return true;
            /*
            Resource.Collection needs = new Resource.Collection();
            
            foreach (var need in item.neededToRepair)
            {
                needs.Give(need, 1);
            }
            
            needs.Substract(p.inventory);

            return (needs.Empty());
            */
        }

        void ConsumeResourcesToShoot()
        {
            /*
            Resource.Collection needs = new Resource.Collection();
            
            foreach (var need in item.neededToRepair)
            {
                needs.Give(need, 1);
            }
            
            p.inventory.Substract(needs);
            
            p.hudView.Refresh();
            */
        }
    }
}