﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.EventSystems;

public class Bullet : DamageObject
{
    private readonly int damageAmount = 2;
    private float speed = 0.12f;

    private Vector2 dir;
    public void Init(Vector2 dir)
    {
        this.dir = dir;
    }
    void Update()
    {
        Move();
    }

    void Move()
    {
        transform.Translate(dir * speed);
        
        if(Mathf.Abs(transform.position.x) > 15f)
            Destroy(gameObject);
    }
}
