﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using RepairPirates;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance;
    public Transform[] leftSpawnPoints;
    public Transform[] rightSpawnPoints;

    public List<Pirate> players = new List<Pirate>();
    public List<Pirate> leftPlayers = new List<Pirate>();
    public List<Pirate> rightPlayers = new List<Pirate>();

    public Transform playersHUD;
    public List<PlayerHUDView> playerHUDs = new List<PlayerHUDView>();

    public GameObject waitingForGameToStart;

    public GameObject winObj;
    public TMP_Text winText;

    public bool isGameStarted = false;
    // Start is called before the first frame update
    void OnPlayerJoined(PlayerInput pInput)
    {
        Pirate p = pInput.gameObject.GetComponentInChildren<Pirate>();
        p.pc.pInput = pInput;
        SpawnPlayer(p,GetTeamForNewPlayer());
        
        players.Add(p);
        
        Debug.Log("Player joined");
        CreatePlayerHUD(p,pInput);
        SoundManager.instance.PlayerJoined();

        if (players.Count == 2)
        {
            waitingForGameToStart.SetActive(false);
            isGameStarted = true;
        }
    }

    public void OnPlayerLeft(PlayerInput pInput)
    {
        RemovePlayerHUD(pInput);
        RemovePirateFromLists(pInput);
    }

    private void RemovePirateFromLists(PlayerInput playerInput)
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].pc.pInput.playerIndex == playerInput.playerIndex)
            {
                players.RemoveAt(i);
                break;
            }
        }
        for (int i = 0; i < leftPlayers.Count; i++)
        {
            if (leftPlayers[i].pc.pInput.playerIndex == playerInput.playerIndex)
            {
                leftPlayers.RemoveAt(i);
                break;
            }
        }
        for (int i = 0; i < rightPlayers.Count; i++)
        {
            if (rightPlayers[i].pc.pInput.playerIndex == playerInput.playerIndex)
            {
                rightPlayers.RemoveAt(i);
                break;
            }
        }
    }

    public void KillPirate(Pirate p)
    {
        StartCoroutine(KillRoutine(p));
    }
    
    IEnumerator KillRoutine(Pirate p)
    {
        //p.GetPlayerController().enabled = false;
        //p.useController.enabled = false;
        
        p.gameObject.SetActive(false);
        yield return new WaitForSeconds(Config.RESPAWN_COOLDOWN);
        p.gameObject.SetActive(true);
        
        p.GetComponentInParent<Rigidbody2D>().velocity = Vector2.zero;
        //p.GetPlayerController().enabled = true;
        //p.useController.enabled = true;
        //Health
        p.currentHealth = p.maxHealth;
        p.hudView.Refresh();
        
        p.gameObject.SetActive(true);
        if (p.team == Team.Green)
        {
            p.gameObject.transform.position = leftSpawnPoints[leftPlayers.Count-1].position;
        }
        else
        {
            p.gameObject.transform.position = rightSpawnPoints[rightPlayers.Count-1].position;
        }
    }

    void CreatePlayerHUD(Pirate pirate, PlayerInput playerInput)
    {
        PlayerHUDView ph = Instantiate(UIPrefabsHolder.instance.playerHUDPrefab, playersHUD);

        switch (playerInput.playerIndex)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                ph.transform.SetSiblingIndex(1);
                break;
        }
        
        ph.Init(pirate,playerInput);
        pirate.hudView = ph;
    }

    void RemovePlayerHUD(PlayerInput playerInput)
    {
        PlayerHUDView[] huds = FindObjectsOfType<PlayerHUDView>();
        for (int i = 0; i < huds.Length; i++)
        {
            if (huds[i].pInput.playerIndex == playerInput.playerIndex)
            {
                Destroy(huds[i].gameObject);
                break;
            }
        }
    }

    void SpawnPlayer(Pirate pirate, Team t)
    {
        Debug.Log("Team: " + t);
        pirate.team = t;
        if (t == Team.Green)
        {
            pirate.gameObject.transform.position = leftSpawnPoints[leftPlayers.Count].position;
            leftPlayers.Add(pirate);
        }
        else
        {
            pirate.gameObject.transform.position = rightSpawnPoints[rightPlayers.Count].position;
            rightPlayers.Add(pirate);
        }
        pirate.Init();
    }

    Team GetTeamForNewPlayer()
    {
        if (players.Count == 0 || players.Count == 2)
        {
            return Team.Green;
        }

        return Team.Red;
    }

    private void Awake()
    {
        waitingForGameToStart.SetActive(true);
        Instance = this;
    }

    public void GameOver(Team winner)
    {
        SoundManager.instance.GameOver();
        winObj.gameObject.SetActive(true);
        winText.text = winner + " WINS!";
        StartCoroutine(RestartGameRoutine());
    }

    IEnumerator RestartGameRoutine()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(0);
        /*
        for (int i = 0; i < players.Count; i++)
        {
            OnPlayerLeft(players[i].pc.pInput);
        }
        */
    }
}
