using System;
using System.Collections;
using Item;
using RepairPirates;
using UnityEngine;
using UnityEngine.UI;

namespace GameManagers
{
    public class BoatsController : MonoBehaviour
    {
        public static BoatsController instance;

        public Transform leftBoat, rightBoat;
        private Repairable[] repairables;
        public Transform broken_0, broken_1, broken_2, broken_3;

        public Sprite left_0, left_1, left_2;
        public Sprite right_0, right_1, right_2;

        public SpriteRenderer leftBoatImage, rightBoatImage;

        private void Awake()
        {
            instance = this;
        }

        void Start()
        {
            repairables = FindObjectsOfType<Repairable>();
        }

        public void OnRepairableStateChanged()
        {
            int leftBroken = CalculateBrokenParts(Team.Green);
            int rightBroken = CalculateBrokenParts(Team.Red);

            switch (leftBroken)
            {
                case 0:
                    StartCoroutine(MoveBoatRoutine(leftBoat, broken_0));
                    leftBoatImage.sprite = left_0;
                    break;
                case 1:
                    StartCoroutine(MoveBoatRoutine(leftBoat, broken_1));
                    leftBoatImage.sprite = left_1;
                    break;
                case 2:
                    StartCoroutine(MoveBoatRoutine(leftBoat, broken_2));
                    leftBoatImage.sprite = left_2;
                    break;
                case 3:
                    StartCoroutine(MoveBoatRoutine(leftBoat, broken_3));
                    GameController.Instance.GameOver(Team.Red);
                    break;
            }
            switch (rightBroken)
            {
                case 0:
                    StartCoroutine(MoveBoatRoutine(rightBoat, broken_0));
                    rightBoatImage.sprite = right_0;

                    break;
                case 1:
                    StartCoroutine(MoveBoatRoutine(rightBoat, broken_1));
                    rightBoatImage.sprite = right_1;
                    break;
                case 2:
                    StartCoroutine(MoveBoatRoutine(rightBoat, broken_2));
                    rightBoatImage.sprite = right_2;
                    break;
                case 3:
                    StartCoroutine(MoveBoatRoutine(rightBoat, broken_3));
                    GameController.Instance.GameOver(Team.Green);
                    break;
            }
        }

        private int CalculateBrokenParts(Team t)
        {
            int brokenParts = 0;
            for (int i = 0; i < repairables.Length; i++)
            {
                if (repairables[i].t == t && repairables[i].Broken())
                {
                    brokenParts++;
                }
            }

            return brokenParts;
        }

        private IEnumerator MoveBoatRoutine(Transform boat, Transform newPos)
        {
            Vector3 targetPos = new Vector3(boat.position.x, newPos.position.y, boat.position.z);
            
            while (Vector3.Distance(boat.position, targetPos) > 0.1f)
            {
                boat.position = Vector3.Lerp(boat.transform.position, targetPos, Time.deltaTime * Config.SINKING_SPEED);
                yield return new WaitForEndOfFrame();
            }
        }
        
    }
}