using System;
using Scriptables;
using UnityEngine;

namespace GameManagers
{
    public class CollectionManager : MonoBehaviour
    {
        public static CollectionManager Instance;
        public InteractuableCollection interactuables;
        public ResourceScriptableCollection resources;

        private void Awake()
        {
            Instance = this;
        }
    }
}