﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Item;
using UnityEngine;

public class CanyonController : MonoBehaviour
{
    bool isInitialized;
    public Projectile ballPrefab;
    public GameObject explosionPrefab;
    public Transform ballSpawnPosition;
    public List<Repairable> dst;

    public Team t;
    // Start is called before the first frame update
    void Start()
    {
        isInitialized = true;
        StartCoroutine(AutoFire());
    }

    public void Shoot()
    {
        if (!CanShoot())
        {
            return;
        }

        GetTarget();

        if(dst == null ||  dst.Count == 0)
            return;
        
        SoundManager.instance.CannonShot();
        Projectile ball = Instantiate(ballPrefab, ballSpawnPosition.transform.position, Quaternion.identity);
        Repairable t = dst[Random.Range(0, dst.Count)];
        Transform target = t.transform;
        float hight = Random.Range(1, 5);
        float speed = Random.Range(3, 8);

        ball.Init(target.position, hight, speed, explosionPrefab, t);
    }

    private void GetTarget()
    {
        Item.Repairable[] targets = FindObjectsOfType<Repairable>();
        dst.Clear();
        for (int i = 0; i < targets.Length; i++)
        {
            if (targets[i].t != t && !targets[i].Broken())
            {
                dst.Add(targets[i]);
            }
        }
    }
    
    public bool CanShoot()
    {
        return GameController.Instance.isGameStarted;
    }
    
    private IEnumerator AutoFire()
    {
        while (true)
        {
            if (isInitialized)
            {
                Shoot();                
            }
            
            yield return new WaitForSeconds(RepairPirates.Config.CANYON_COOLDOWN);
        }
    }
}
