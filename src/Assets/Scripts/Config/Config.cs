namespace RepairPirates
{
    public static class Config
    {
        public const float JUMP_FORCE = 7f;

        // Lateral movement speed
        public const float LATERAL_MOVEMENT = 3f;

        // Spawning time (seconds)
        public const float RESOURCES_DELAY = 2f;
        
        // Time-To-Live for non-picked resources. 
        public const float RESOURCES_TTL = 7f;

        // Time the player is frozen and cannot move (seconds)
        public const float BLOCK_TIME = 0.3f;

        // Time the canyon is blocked after firing. 
        public const float CANYON_COOLDOWN = 3f;

        public const float RESPAWN_COOLDOWN = 7f;

        public const float EXPLOSION_TTL = 3f;

        public const float SINKING_SPEED = 1f;
    }
}