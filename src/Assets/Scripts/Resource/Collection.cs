using System.Collections.Generic;
using UnityEngine;

namespace Resource
{
      public class Collection
      {
            public Dictionary<Type, int> resources = new Dictionary<Type, int>();

            public int Has(Type type)
            {
                  return resources.ContainsKey(type) ? resources[type] : 0;
            }

            public void Give(Type type, int amount)
            {
                  if (!resources.ContainsKey(type))
                  {
                        resources.Add(type, amount);
                        return;
                  }

                  resources[type] += amount;
            }

            public void Give(Collection c)
            {
                  foreach (var res in c.resources)
                  {
                        Give(res.Key, res.Value);
                  }
            }

            public void Substract(Type type, int amount)
            {
                  if (!resources.ContainsKey(type))
                        return;
                  
                  resources[type] -= amount;
                  if (resources[type] <= 0)
                  {
                        resources.Remove(type);
                  }
            }

            public void Substract(Collection c)
            {
                  foreach (var res in c.resources)
                  {
                        Substract(res.Key, res.Value);
                  }
            }

            public bool Empty()
            {
                  return resources.Count == 0;
            }
      }
}