namespace Resource
{
    public enum Type
    { 
        Gunpowder,
        CanyonBall,
        Wood,
        NailsPack,
        Iron,
        Hammer,
        Fire,
        Parrot,
    }
}

