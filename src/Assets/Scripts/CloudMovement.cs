﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMovement : MonoBehaviour
{

    float speed;
    // Start is called before the first frame update
    void Start()
    {
        speed = Random.Range(0.5f, 4f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(1f, 0f) * Time.deltaTime * speed);
        if(transform.position.x > 12f)
        {
            Destroy(gameObject);
        }
    }
}
