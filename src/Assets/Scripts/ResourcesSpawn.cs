﻿using System.Collections;
using System.Collections.Generic;
using GameManagers;
using UnityEngine;
using RepairPirates;
using Scriptables;

public class ResourcesSpawn : MonoBehaviour
{

    public bool isInitialized;
    public GameObject spawnObject;

    // Start is called before the first frame update
    void Start()
    {
        isInitialized = true;
        StartCoroutine(LoopResources());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Resources loop
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoopResources()
    {
        while (true)
        {
            if (isInitialized && GameController.Instance.isGameStarted)
            {
                SpawnResource();                
            }
            
            yield return new WaitForSeconds(Config.RESOURCES_DELAY);
        }        
    }

    /// <summary>
    /// Retreive the current spawning points
    /// </summary>
    /// <returns></returns>
    private Transform GetRandomSpawnPoint()
    {
        return transform.GetChild(Random.Range(0, transform.childCount));
    }

    /// <summary>
    /// Spawn a random resource
    /// </summary>
    /// 
    /// TODO: Spawn a random resource
    public void SpawnResource()
    {
        Vector2 t_parent = GetRandomSpawnPoint().position;
        ResourceCollect rc = Instantiate(spawnObject, t_parent, Quaternion.identity).GetComponent<ResourceCollect>();

        int amount = Random.Range(1, 3);
        ResourceScriptable resource = CollectionManager.Instance.resources.GetRandomResource();
        
        rc.Init(resource.type, amount, resource.sprite);
    }
}
