﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using Item;

// Author: http://luminaryapps.com/blog/arcing-projectiles-in-unity/
public class Projectile : DamageObject {
    public Vector3 targetPos;
    public float speed = 10;
    
    private float arcHeight = 1;
    private Vector3 startPos;
    private GameObject explosionPrefab;
    private Repairable target;

    public void Init(Vector3 dst, float height, float speed, GameObject explosion, Repairable target)
    {
        this.target = target;
        targetPos = dst;
        arcHeight = height;
        speed = speed;
        explosionPrefab = explosion;
    }

    void Start()
    {
        // Cache our start position, which is really the only thing we need
        // (in addition to our current position, and the target).
        startPos = transform.position;
    }
    
    void Update()
    {
        // Compute the next position, with arc added in
        float x0 = startPos.x;
        float x1 = targetPos.x;
        float dist = x1 - x0;
        float nextX = Mathf.MoveTowards(transform.position.x, x1, speed * Time.deltaTime);
        float baseY = Mathf.Lerp(startPos.y, targetPos.y, (nextX - x0) / dist);
        float arc = arcHeight * (nextX - x0) * (nextX - x1) / (-0.25f * dist * dist);
        Vector3 nextPos = new Vector3(nextX, baseY + arc, transform.position.z);
		
        // Rotate to face the next position, and then move there
        transform.rotation = LookAt2D(nextPos - transform.position);
        transform.position = nextPos;
		
        // Do something when we reach the target
        if (nextPos == targetPos) Arrived();
    }
	
    void Arrived()
    {
        var fx = Instantiate(explosionPrefab, targetPos, Quaternion.identity).GetComponent<ExplosionController>();
        fx.Init();
        target.TakeDamage(1);
        target.hud.Refresh();
        
        Destroy(gameObject);
    }
	
    /// 
    /// This is a 2D version of Quaternion.LookAt; it returns a quaternion
    /// that makes the local +X axis point in the given forward direction.
    /// 
    /// forward direction
    /// Quaternion that rotates +X to align with forward
    static Quaternion LookAt2D(Vector2 forward) {
        return Quaternion.Euler(0, 0, Mathf.Atan2(forward.y, forward.x) * Mathf.Rad2Deg);
    }
}